## MongoDB using docker compose with authorization

## Usage

1. Clonse this repo
    > You'll get mongo folder
2. Create your ```mongo.env``` with required environment values
3. Make setupmongo.sh runnable:
    > ```$ chmod +x setupmongo.sh ```

4. ```$ docker-compose up ``` inside mongo folder

<hr>

> Make sure to change ```/root/``` in docker-compose.yml to your mongo directory path

### Required env values

```bash
MONGODB_USER=<your-db-user>
MONGODB_PASS=<your-db-password>
MONGODB_DBNAME=<your-db>
MONGO_INITDB_ROOT_USERNAME=<admin-db-user>
MONGO_INITDB_ROOT_PASSWORD=<admin-db-password>
```

## Next steps
- add clustering
- add servier to handle primary cluster change

## Contributing

Please submit all issues and pull requests to the [azeke13/docker-compose-mongodb](https://gitlab.com/azeke13/docker-compose-mongodb) repository!

## Support

If you have any problem or suggestion please open an issue [here](https://gitlab.com/azeke13/docker-compose-mongodb/-/issues).
