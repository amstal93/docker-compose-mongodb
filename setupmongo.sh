#!/bin/bash
mongodb=`getent hosts ${MONGO} | awk '{ print $1 }'`

port=${PORT:-27017}

echo "----------------------------------------"
echo "Waiting for startup.."
echo "mongo: " ${mongodb}
echo "port: " ${port}

until mongo --host ${mongodb}:${port} --eval 'quit(db.runCommand({ ping: 1 }).ok ? 0 : 2)' &>/dev/null; do
    printf '.'
    sleep 1
done
echo "Started.."
echo "----------------------------------------"

echo "----------------------------------------"
echo "Create User"
echo ${MONGODB_DBNAME}: ${MONGODB_USER}, ${MONGODB_PASS}
echo setup user auth time now: `date +"%T" `
mongo --host ${mongodb}:${port} -u ${MONGO_INITDB_ROOT_USERNAME} --password ${MONGO_INITDB_ROOT_PASSWORD} <<EOF
    use ${MONGODB_DBNAME}
    db.createUser(
        {
            "user": "${MONGODB_USER}",
            "pwd": "${MONGODB_PASS}",
            "roles": [ "readWrite"]
        }
    )
EOF
echo "User created"
echo "----------------------------------------"
